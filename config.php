<?php

$dbhost = ""; //mysql database host
$dbname = ""; //mysql database name
$dbusername = ""; //mysql database username
$dbpassword = ""; //mysql database password

$webTitle = "DayZ Admin & User Panel";
$loginMessage = "Please log in to access this feature.";
$accessError = "You are not authorised to view this page.";

//DO NOT EDIT BEYOND THIS POINT

$DB_CONNSTRING = "mysql:host="; // DO NOT EDIT THIS LINE
$DB_CONNSTRING .= $dbhost; // THIS SHOULD BE THE DATABASE HOST:PORT (E.g. localhost:3306 or 127.0.0.1:3306 or yourdomain.net:3306)
$DB_CONNSTRING .= ";dbname="; // DO NOT EDIT THIS LINE
$DB_CONNSTRING .= $dbname; // EDIT THIS LINE, THIS SHOULD BE THE DATABASE NAME
$DB_USERNAME = $dbusername; // EDIT THIS LINE, THIS SHOULD BE THE DATABASE USERNAME
$DB_PASSWORD = $dbpassword; // EDIT THIS LINE, THIS SHOULD BE THE DATABASE PASSWORD