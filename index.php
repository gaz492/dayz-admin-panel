
<?php

session_start();

require_once("config.php");
$dbh = new PDO($DB_CONNSTRING, $DB_USERNAME, $DB_PASSWORD);
require_once("includes/check.php");

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>DayZ User Panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }

    </style>
      <link href="assets/css/custom.css" rel="stylesheet">
      <link href="assets/css/bootstrap-select.css" rel="stylesheet">
      <script src="./assets/js/weaponImg.js"></script>


  </head>

  <body>

  <!-- Fixed navbar -->
  <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">DayZ Admin & User</a>
          </div>
          <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                  <?php include_once("menu.php"); ?>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li class=""><a href="#"><?php if ($loggedin === true){ ?> Logged-in as <?php echo $_SESSION["username"] . " / " . $_SESSION["adminid"]; }else {  }?></a></li>
              </ul>
          </div><!--/.nav-collapse -->
      </div>
  </div>

    <div class="container mainCol">

        <?php

            $pages = array("login", "edit","admin","admin/edit","admin/players","admin/pedit","admin/settings","admin/vip");
            if (isset($_GET["p"]) AND in_array($_GET["p"], $pages)) {

                include_once ("pages/".$_GET["p"].".php");

            } else {

                include_once ("pages/home.php");

            }

            ?>

    </div> <!-- /container -->
    <footer>
        <p style="text-align: center">By using this website you agree to use cookies</p>
    </footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="./assets/js/transition.js"></script>
    <script src="./assets/js/alert.js"></script>
    <script src="./assets/js/modal.js"></script>
    <script src="./assets/js/scrollspy.js"></script>
    <script src="./assets/js/tab.js"></script>
    <script src="./assets/js/tooltip.js"></script>
    <script src="./assets/js/popover.js"></script>
    <script src="./assets/js/button.js"></script>
    <script src="./assets/js/collapse.js"></script>
    <script src="./assets/js/carousel.js"></script>
    <script src="./assets/js/bootstrap.js"></script>
    <script src="./assets/js/bootstrap-select.js"></script>


  </body>
</html>
