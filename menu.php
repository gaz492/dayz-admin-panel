<li><a href="./">Home</a></li>
<li><?php if ($loggedin=== true) { ?><a href="./?p=edit">Starting Loadout</a><?php } ?></li>
<li class="dropdown"><?php if ($loggedin === true AND rights("admin")) { ?><a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin Tools<b class="caret"></b></a><?php } ?>
	<ul class="dropdown-menu">
		<li><?php if ($loggedin=== true AND rights("admin")) { ?><a href="./?p=admin/edit">Global Loadout</a><?php } ?></li>
		<li><?php if ($loggedin=== true AND rights("admin")) { ?><a href="./?p=admin/vip">VIP Management</a><?php } ?></li>
		<li><?php if ($loggedin=== true AND rights("admin")) { ?><a href="./?p=admin/settings">Settings</a><?php } ?></li>
        <li><?php if ($loggedin=== true AND rights("admin")) { ?><a href="#">More to come</a><?php } ?></li>
	</ul>
</li>
<li><?php if ($loggedin) { ?><a href="./?p=login&out=1">Logout</a><?php } ?></li>
