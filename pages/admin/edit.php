<h1>Global Loadout</h1>

<?php

if ($loggedin === false) { echo "Please log in to edit the Global loadout."; header('Refresh: 2; URL=?p=login'); }
elseif (rights("admin")) { ?>

    <h2>Global Starting Loadout Editor</h2>

    <form class="form-inline">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Editing: Global Loadout</th>
                <th>Selection</th>
                <th>Image</th>
            </tr>
            </thead>
            <tr>
                <td>Primary Weapon</td>
                <td>
                    <select class="form-control selectpicker" data-style="btn-primary" id='primaryCombo' name='primaryCombo' onChange='activeImage()'>

                        <optgroup label='Shotgun'>
                            <option value='Remington870_lamp' >Remington 870</option>
                            <option value='Saiga12K' >Saiga 12K</option>
                            <option value='Winchester1866' >Winchester 1866</option>
                        </optgroup>

                        <optgroup label='Assault Rifle'>
                            <option value='AK_107_kobra' >AK-107</option>
                            <option value='AK_107_GL_kobra' >AK-107 / GP25</option>
                            <option value='AK_107_pso' >AK-107 PSO</option>
                            <option value='AK_74' >AK-74</option>
                            <option value='AK_74_GL' >AK-74 / GP25</option>
                            <option value='AK_74_GL_kobra' >AK-74 GP-25 Kobra</option>
                            <option value='AK_107_GL_pso' >AK107 / GP25 PSO</option>
                            <option value='AK_47_M' >AKM</option>
                            <option value='AK_47_S' >AKS</option>
                            <option value='AKS_GOLD' >AKS GOLD</option>
                            <option value='AKS_74' >AKS-74</option>
                            <option value='AKS_74_kobra' >AKS-74 Kobra</option>
                            <option value='AKS_74_pso' >AKS-74 PSO</option>
                            <option value='AKS_74_U' >AKS-74U</option>
                            <option value='AKS_74_UN_kobra' >AKS-74UN Kobra</option>
                            <option value='Crossbow' >Crossbow</option>
                            <option value='FN_FAL' >FN FAL</option>
                            <option value='FN_FAL_ANPVS4' >FN FAL ANPVS4</option>
                            <option value='G36a' >G36</option>
                            <option value='G36C' >G36C</option>
                            <option value='G36_C_SD_eotech' >G36C Eotech SD</option>
                            <option value='G36_C_SD_camo' >G36C SD (camo)</option>
                            <option value='G36K' >G36K</option>
                            <option value='G36K_camo' >G36K (camo)</option>
                            <option value='BAF_L85A2_RIS_ACOG' >L85A2 Acog</option>
                            <option value='BAF_L85A2_UGL_ACOG' >L85A2 Acog GL</option>
                            <option value='BAF_L85A2_RIS_CWS' >L85A2 AWS</option>
                            <option value='BAF_L85A2_RIS_Holo' >L85A2 Holo</option>
                            <option value='BAF_L85A2_UGL_Holo' >L85A2 Holo GL</option>
                            <option value='BAF_L85A2_RIS_SUSAT' >L85A2 SUSAT</option>
                            <option value='BAF_L85A2_UGL_SUSAT' >L85A2 SUSAT GL</option>
                            <option value='BAF_L86A2_ACOG' >L86A2 LSW</option>
                            <option value='M1014' >M1014</option>
                            <option value='M16A2' >M16A2</option>
                            <option value='M16A2GL' >M16A2 / M203</option>
                            <option value='M16A4' >M16A4</option>
                            <option value='M16A4_GL' >M16A4 / M203</option>
                            <option value='M16A4_ACG_GL' >M16A4 / M203 RCO</option>
                            <option value='M16A4_ACG' >M16A4 RCO</option>
                            <option value='M4A1' >M4A1</option>
                            <option value='M4A1_HWS_GL' >M4A1 / M203 Holo</option>
                            <option value='M4A1_HWS_GL_camo' >M4A1 / M203 Holo Camo</option>
                            <option value='M4A1_HWS_GL_SD_Camo' >M4A1 / M203 Holo SD</option>
                            <option value='M4A1_RCO_GL' >M4A1 / M203 RCO</option>
                            <option value='M4A1_Aim' >M4A1 CCO</option>
                            <option value='M4A1_Aim_camo' >M4A1 CCO Camo</option>
                            <option value='M4A1_AIM_SD_camo' >M4A1 CCO Camo SD</option>
                            <option value='M4A3_RCO_GL_EP1' >M4A3 / M203 Acog</option>
                            <option value='M4A3_CCO_EP1' >M4A3 CCO</option>
                            <option value='SCAR_L_CQC_CCO_SD' >Mk16 CCO SD</option>
                            <option value='SCAR_L_CQC' >Mk16 CQC</option>
                            <option value='SCAR_H_CQC_CCO' >Mk17 CCO</option>
                            <option value='SCAR_H_STD_EGLM_Spect' >Mk17 EGLM RCO</option>
                        </optgroup>

                        <optgroup label='Light Machine Gun'>
                            <option value='G36A_camo' >G36A (camo)</option>
                            <option value='BAF_L110A1_Aim' >L110A1</option>
                            <option value='BAF_L7A2_GPMG' >L7A2 GPMG</option>
                            <option value='M240' >M240</option>
                            <option value='m240_scoped_EP1' >M240 Scope</option>
                            <option value='M249' >M249</option>
                            <option value='M249_EP1' >M249 SAW</option>
                            <option value='M249_m145_EP1' >M249 Scope</option>
                            <option value='M60A4_EP1' >M60E4</option>
                            <option value='Mk_48_DES_EP1' >Mk 48 Desert</option>
                            <option value='Mk_48' >Mk 48 Mod 0</option>
                            <option value='RPK_74' >RPK-74</option>
                        </optgroup>

                        <optgroup label='Sub Machine Gun'>
                            <option value='Bizon' >Bizon PP-19</option>
                            <option value='bizon_silenced' >Bizon PP-19 SD</option>
                            <option value='MP5A5' >MP5A5</option>
                            <option value='MP5SD' >MP5SD6</option>
                        </optgroup>

                        <optgroup label='Sniper Rifle'>
                            <option value='BAF_AS50_scoped'  >AS50 </option>
                            <option value='Huntingrifle' >CZ 550 Scoped</option>
                            <option value='DMR'>DMR</option>
                            <option value='BAF_LRR_scoped_W' >L115A2 LRR W</option>
                            <option value='BAF_LRR_scoped' >L115A3 LRR</option>
                            <option value='LeeEnfield' >Lee Enfield</option>
                            <option value='m107'  >M107 </option>
                            <option value='M14_EP1' >M14 Aimpoint</option>
                            <option value='M24' >M24</option>
                            <option value='M24_des_EP1' >M24 Desert</option>
                            <option value='M40A3' >M40A3</option>
                            <option value='M4SPR' >Mk12 SPR</option>
                            <option value='SCAR_H_LNG_Sniper' >Mk17 Sniper</option>
                            <option value='SCAR_H_LNG_Sniper_SD' >Mk17 Sniper SD</option>
                            <option value='SVD' >SVD</option>
                            <option value='SVD_CAMO' >SVD Camo</option>
                        </optgroup>
                    </select>
                </td>
                <td>
                    <img id="primaryImg" style="max-width:280px;max-height:200px;" onload="activeImage()" src="./assets/img/wep/AK_107_GL_kobra.png" alt="Image has been lost in transit :(">
                </td>
            </tr>
            <tr>
                <td>Secondary Weapon</td>
                <td>
                    <select class="form-control selectpicker" data-style="btn-primary" id='secondaryCombo' name='secondaryCombo' onChange='activeImage()'>
                            <option value="glock17_EP1">G17</option>
                            <option value="Colt1911">M1911</option>
                            <option value="M9">M9</option>
                            <option value="M9SD">M9 Silenced</option>
                            <option value="Makarov">Makarov PM</option>
                            <option value="MakarovSD">Makarov Silenced</option>
                            <option value="revolver_EP1">Revolver</option>
                            <option value="revolver_gold_EP1" selected="selected">Revolver Gold</option>
                            <option value="UZI_EP1">UZI (PDW)</option>
                            <option value="UZI_SD_EP1">UZI SD</option>
                        </select>
                </td>
                <td>
                    <img id="secondaryImg" style="max-width:280px;max-height:200px;" onload="activeImage()" src="./assets/img/wep/sec/Colt1911.png" alt="Image has been lost in transit :(">
                </td>
            </tr>

            <tr>
                <td>Backpack</td>
                <td>
                    <select class="form-control selectpicker" data-style="btn-primary" id='secondaryCombo' name='secondaryCombo' onChange='activeImage()'>
                        <option value="glock17_EP1">G17</option>
                        <option value="Colt1911">M1911</option>
                        <option value="M9">M9</option>
                        <option value="M9SD">M9 Silenced</option>
                        <option value="Makarov">Makarov PM</option>
                        <option value="MakarovSD">Makarov Silenced</option>
                        <option value="revolver_EP1">Revolver</option>
                        <option value="revolver_gold_EP1" selected="selected">Revolver Gold</option>
                        <option value="UZI_EP1">UZI (PDW)</option>
                        <option value="UZI_SD_EP1">UZI SD</option>
                    </select>
                </td>
                <td>
                    <img id="secondaryImg" style="max-width:280px;max-height:200px;" onload="activeImage()" src="./assets/img/back/Colt1911.png" alt="Image has been lost in transit :(">
                </td>
            </tr>
        </table>
    </form>

<?php } else  echo $accessError . header('Refresh: 2; URL=?p=home');?>