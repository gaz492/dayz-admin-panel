<h1>Settings</h1>

<?php
if ($loggedin === false) { echo "Please log in to edit your loadout."; include_once("login.php"); }
elseif (rights("admin")) {

?>

    <h2>Global Starting Loadout Editor</h2>

    <form class="form-inline" role="form" method="get" action="./">
        <table class="table table-bordered table-striped">
            <thead><tr><th>Settings</th></tr></thead>
            <tr>
                <td>Server Name</td>
                <br />
                <td>
                    <div class="form-group">
                        <div class="controls">
                            <input class="form-control" widt type="text" name="news" placeholder="Enter your server name"/>
                            <br />
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    Server MOTD
                </td>
                <td>
                    <textarea class="form-control">Coming Soon</textarea>
                </td>
            </tr>
            <tr>
                <td>
                    <button class="btn btn-primary" name="submitbutton">Save</button></td>
                <td>
                </td>
            </tr>
        </table>
    </form>

<?php } else  echo "You are not an admin." . header('Refresh: 2; URL=?p=home');; ?>