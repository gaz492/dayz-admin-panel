<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
$news = "";

if ($loggedin === false) { echo "Please log in to access the admin panel."; header('Refresh: 2; URL=?p=login'); }
else {

    $modellist = array("SurvivorW2_DZ" => "Woman", "BanditW1_DZ" => "Woman (bandit)", "Survivor2_DZ" => "Survivor", "Survivor3_DZ" => "Hero", "Sniper1_DZ" => "Ghillie suit", "Camo1_DZ" => "Camo suit", "Bandit1_DZ" => "Bandit", "Soldier1_DZ" => "Soldier", "Rocket_DZ" => "Rocket (red barret)");
    $uid = $_SESSION['adminid'];

    if(isset($_REQUEST['addPackageBtn'])) {
        $packageName = $_POST['inputName'];
        $packageInventory = $_POST['inputInventory'];
        $packageBackpack = $_POST['inputBackpack'];
        $packageSkin = $_POST['inputSkin'];

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stm = $dbh->prepare("INSERT INTO `cust_loadout`(`inventory`, `backpack`, `model`, `description`) VALUES (?, ?, ?, ?)");
        $stm->execute( array($packageInventory, $packageBackpack, $packageSkin, $packageName) );
    }

    if(isset($_REQUEST['addVIPBtn'])) {
        $packageName = $_POST['packageDesc'];
        $userUniqueId = $_POST['uniqueID'];

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stm = $dbh->prepare("INSERT INTO `cust_loadout_profile` (`cust_loadout_id` ,`unique_id`)VALUES (?, ?);");
        $stm->execute( array($packageName, $userUniqueId) );
    }

    if(isset($_GET['deleteVip'])) {

        $delID = $_GET['deleteVip'];
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stm = $dbh->prepare("DELETE FROM cust_loadout_profile WHERE unique_id = ?");
        $stm->execute( array($delID) );
        header('Refresh: 0; URL=?p=admin/vip');
    }

    if(isset($_GET['deletePackage'])) {

        $delID = $_GET['deletePackage'];
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stm = $dbh->prepare("DELETE FROM cust_loadout WHERE id = ?");
        $stm->execute( array($delID) );
        header('Refresh: 0; URL=?p=admin/vip');
    }

    ?>
    <h1>Manage VIP's</h1>

    <h2>VIP's</h2>
    <a data-toggle="modal" href="#addVIP" class="btn btn-primary">Add VIP</a>
    <br/>
    <br/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Delete</th>
            <th>Unique ID</th>
            <th>Package</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($dbh->query("SELECT * FROM cust_loadout_profile") AS $vip) {

        echo "<tr>";
        //echo "<td><button class='btn btn-primary' type='submit' name='deleteVipBtn' value=" . $vip['cust_loadout_id'] . ">Delete</button></td>";
        echo "<td><a href='?p=admin/vip&deleteVip=". $vip['unique_id'] . "' role='button' class='btn btn-primary'>Delete</a></td>";
        echo "<td>".$vip["unique_id"]."</td>";
        echo "<td>".$vip["cust_loadout_id"]."</td>";
        echo "</tr>";


        }
    ?>
        </tbody>
    </table>


    <h2>VIP Packages</h2>
    <a data-toggle="modal" href="#addPackage" class="btn btn-primary">Add VIP Package</a>
    <br/>
    <br/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Delete</th>
            <th>ID</th>
            <th>Name</th>
            <th>Inventory</th>
            <th>Backpack</th>
            <th>Skin</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($dbh->query("SELECT * FROM cust_loadout") AS $vip) {

            echo "<tr>";
            echo "<td><a href='?p=admin/vip&deletePackage=". $vip['id'] . "' role='button' class='btn btn-primary'>Delete</a></td>";
            echo "<td>".$vip["id"]."</td>";
            echo "<td>".$vip["description"]."</td>";
            echo "<td><textarea class='form-control' disabled rows='4' style='width: 100%; color:black;'>".$vip["inventory"]."</textarea></td>";
            echo "<td><textarea class='form-control' disabled rows='4' style='width: 100%; color:black;'>".$vip["backpack"]."</textarea></td>";
            echo "<td>".$vip["model"]."</td>";
            echo "</tr>";


        }
        ?>
        </tbody>
    </table>

    <div class="modal fade" id="addVIP" tabindex="-1" role="dialog" aria-labelledby="addVIPLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add VIP</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" acion="./?p=admin/vip" role="form" method="post">
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="inputUniqueID">Unique ID</label>
                            <div class="col-lg-7">
                                <select class="form-control selectpicker" name="uniqueID" id="inputUniqueID" placeholder="Unique ID">
                                    <?php
                                    foreach ($dbh->query("SELECT * FROM survivor where is_dead = 0") AS $userID) {
                                        echo "<option>" . $userID['unique_id'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="inputPackage">Package</label>
                            <div class="col-lg-7">
                                <select class="form-control selectpicker" name="packageDesc" id="inputUniqueID" placeholder="Unique ID">
                                    <?php
                                    foreach ($dbh->query("SELECT * FROM cust_loadout") AS $packageID) {
                                        echo "<option>" . $packageID['id'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="addVIPBtn" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Add VIP Package Modal -->
    <div class="modal fade" id="addPackage" tabindex="-1" role="dialog" aria-labelledby="addPackageLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Package</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form" acion="./?p=admin/vip" method="post">
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="inputName">Name</label>

                            <div class="col-lg-7">
                                <input class="form-control" type="text" id="inputName" name="inputName" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="inputInventory">Inventory</label>

                            <div class="col-lg-7">
                                <input class="form-control" type="text" id="inputInventory" name="inputInventory" placeholder="Inventory">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="inputBackpack">Backpack</label>

                            <div class="col-lg-7">
                                <input class="form-control" type="text" id="inputBackpack" name="inputBackpack" placeholder="Backpack"
                                       value='["DZ_Patrol_Pack_EP1",[[],[]],[[],[]]]'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label" for="inputSkin">Skin</label>

                            <div class="col-lg-5">
                                <select class="form-control selectpicker" data-style="btn-primary" id="inputSkin" name="inputSkin">
                                    <?php
                                    foreach ($modellist AS $key => $value) {

                                        echo "<option value='" . ($key) . "'" . ">$value" . "</option>";

                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <p>Click <a href="./pages/loadoutS/loadout.php" onclick="window.open(this.href, 'mywin','left=20,top=20,width=750px,height=550px,toolbar=0,resizable=0'); return false;">here</a> to generate a inventory string</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="addPackageBtn" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
}

?>