<h1>Loadout</h1>

<?php

if ($loggedin === false) { echo $loginMessage; include_once("login.php"); }
else {

    /*$stm = $dbh->query("SELECT id,description FROM cust_loadout WHERE description NOT LIKE 'Admin%'");
    while ($preLoadout = $stm)
    $loadoutList[] = $preLoadout;*/
    $uid = $_SESSION["adminid"];

    if(isset($_GET['deleteVip'])) {

        $delID = $_GET['deleteVip'];
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stm = $dbh->prepare("DELETE FROM cust_loadout_profile WHERE unique_id = ?");
        $stm->execute( array($delID) );
    }

    if(isset($_REQUEST['addVIPBtn'])) {
        $packageName = $_POST['packageDesc'];
        $userUniqueId = $_POST['uniqueID'];

        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stm = $dbh->prepare("INSERT INTO `cust_loadout_profile` (`cust_loadout_id` ,`unique_id`)VALUES (?, ?);");
        $stm->execute( array($packageName, $userUniqueId) );
    }
    ?>

<h2>Starting Loadout Editor</h2>

    <h2>Set loadout</h2>
    <a href="#selectLoadout" role="button" data-toggle="modal" class="btn btn-primary"><i class="icon-white icon-plus-sign"></i> Select Loadout</a>
    <br/>
    <br/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Delete</th>
            <th>Unique ID</th>
            <th>Package</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($dbh->query("SELECT * FROM cust_loadout_profile WHERE unique_id = '" . $uid . "'") AS $vip) {

            echo "<tr>";
            //echo "<td><button class='btn btn-primary' type='submit' name='deleteVipBtn' value=" . $vip['cust_loadout_id'] . ">Delete</button></td>";
            echo "<td><a href='?p=edit&deleteVip=". $vip['unique_id'] . "' role='button' class='btn btn-primary'>Delete</a></td>";
            echo "<td>".$vip["unique_id"]."</td>";
            echo "<td>".$vip["cust_loadout_id"]."</td>";
            echo "</tr>";


        }
        ?>
        </tbody>
    </table>

    <form class="form-inline">
        <h2>Available Loadouts</h2>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Inventory</th>
                <th>Backpack</th>
                <th>Skin</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($dbh->query("SELECT * FROM cust_loadout WHERE description NOT LIKE 'Admin%'") AS $vip) {

                echo "<tr>";
                echo "<td>".$vip["id"]."</td>";
                echo "<td>".$vip["description"]."</td>";
                echo "<td><textarea readonly class='form-control' rows='3' style='color:black'>".$vip["inventory"]."</textarea></td>";
                echo "<td><textarea readonly class='form-control' rows='3' style='color:black'>".$vip["backpack"]."</textarea></td>";
                echo "<td>".$vip["model"]."</td>";
                echo "</tr>";


            }
            ?>
            </tbody>
        </table>
    </form>

    <!-- Select Loadout Modal -->
    <div class="modal fade" id="selectLoadout" tabindex="-1" role="dialog" aria-labelledby="selectLoadoutLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Select Loadout</h4>
                </div>
                <div class="modal-body">
            <form class="form-horizontal" acion="./?p=admin/vip" role="form" method="post">
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="inputUniqueID">Unique ID</label>
                    <div class="col-lg-10">
                        <input style="color: black;" class="form-control" type="text" name="uniqueID" id="inputUniqueID" readonly value="<?php echo $uid; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label" for="inputPackage">Package</label>
                    <div class="col-lg-10">
                        <select class="form-control selectpicker" name="packageDesc" id="inputUniqueID" placeholder="Unique ID">
                            <?php
                            foreach ($dbh->query("SELECT * FROM cust_loadout WHERE description NOT LIKE 'Admin%'") AS $packageID) {
                                echo "<option>" . $packageID['id'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" name="addVIPBtn" class="btn btn-primary">Save changes</button>
            </form>
        </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<?php } ?>