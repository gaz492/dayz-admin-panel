<h1>Loadout</h1>

<?php
if ($loggedin === false) { echo "Please log in to edit your loadout."; include_once("login.php"); }
else {

    $modellist = array("SurvivorW2_DZ" => "Woman", "BanditW1_DZ" => "Woman (bandit)", "Survivor2_DZ" => "Survivor", "Survivor3_DZ" => "Hero", "Sniper1_DZ" => "Ghillie suit", "Camo1_DZ" => "Camo suit", "Bandit1_DZ" => "Bandit", "Soldier1_DZ" => "Soldier", "Rocket_DZ" => "Rocket (red barret)");
    ?>

    <h2>Starting Loadout Editor</h2>

    <form class="form-inline">
        <table class="table table-bordered table-striped">
            <thead><tr><th>Editing: <?php echo $_SESSION["username"] . " / " . $_SESSION["adminid"]; ?></th></tr></thead>
            <tr>
                <td>Loadout Editor</td>
                <td>
                    <div class="control-group">
                        <label class="control-label"></label>
                        <div class="controls">
                            <iframe width="726px" height="481px" frameBorder="0" src="./pages/loadout/loadout.php"></iframe><br><b><code>Made by Ersan, Modded by Gaz492</code></b></center>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Skin</td>
                <td>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Skin</label>
                        <div class="controls">

                            <select class="selectpicker" name="model">
                                <?php
                                foreach ($modellist AS $key => $value) {

                                    echo "<option value='".($survivor["model"]==$key?"0":$key)."'".($survivor["model"]==$key?" selected":"").">$value".($survivor["model"]==$key?" - Current":"")."</option>";

                                }
                                ?>
                            </select>
                            <br />
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </form>

<?php } ?>