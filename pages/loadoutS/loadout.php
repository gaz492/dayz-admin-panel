<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
require_once("../../config.php");
session_start();
$instanceid = 1; //instance id
global $loadout_id;

mysql_connect($dbhost, $dbusername, $dbpassword, true) or die("Could not connect to mysql: ".mysql_error());
mysql_select_db($dbname) or die("Could not select database: ".mysql_error());

$uid = $_SESSION["adminid"];
$isID = mysql_query("SELECT * FROM cust_loadout_profile WHERE unique_id = " . $uid);
$isID = mysql_fetch_array($isID);
$hasID = $isID['cust_loadout_id'];

if(isset($_GET['save'])) {
    $uid = $_SESSION["adminid"];
    $result_id = mysql_query("SELECT `cust_loadout_id` FROM `cust_loadout_profile` WHERE `unique_id` = " . $uid);
    $result_id = mysql_fetch_array($result_id);
    $loadout_id = $result_id['cust_loadout_id'];
mysql_query("UPDATE cust_loadout SET inventory='".mysql_real_escape_string($_GET['string'])."' WHERE id = " . $loadout_id);
mysql_close();
print "1";
} else {
?>
<head>
<title>Loadout Editor</title>
<style>
    a:link {color: rgb(235,235,235); text-decoration: none; }
    a:active {color: rgb(163, 163, 163); text-decoration: none; }
    a:visited {color: rgb(235,235,235); text-decoration: underline; }
    a:hover {color: #ff0000; text-decoration: none; }
</style>
<script src="jquery.min.js" type="text/javascript"></script>
<script>
var num_weaponprimary = 0;
var num_weaponsecondary = 0;
var num_inventory = 0;
var num_secinventory = 0;
var num_tool = 0;
var num_backpack = 0;
var num_head = 0;
var primarystring = "";
var secondarystring = "";
var infodisplayed = false;
function additem(type, classname, slots) {
if(typeof slots == 'undefined') slots = 1;
	switch(type) {
		case "head":
			if(num_head >= 2) { break; }
			if(primarystring.indexOf(classname, 0) != -1) { break; }
			num_head++;
			$('#head_' + num_head).html("<img onclick=\"delitem('head', " + num_head + ", '" + classname + "')\" style='cursor:pointer; cursor:hand;' src='./items/" + classname + ".png'>");
			primarystring = primarystring + "\"" + classname + "\",";
			break;
		case "weaponprimary":
			if(num_weaponprimary >= 1) { break; }
			num_weaponprimary++;
			$('#weaponprimary').html("<img onclick=\"delitem('weaponprimary', " + num_weaponprimary + ", '" + classname + "')\" style='cursor:pointer; cursor:hand;' src='./items/" + classname + ".png'>");
			primarystring = primarystring + "\"" + classname + "\",";
			break;
		case "weaponsecondary":
			if(num_weaponsecondary >= 1) { break; }
			num_weaponsecondary++;
			$('#weaponsecondary').html("<img onclick=\"delitem('weaponsecondary', " + num_weaponsecondary + ", '" + classname + "')\" style='cursor:pointer; cursor:hand;' src='./items/" + classname + ".png'>");
			primarystring = primarystring + "\"" + classname + "\",";
			break;
		case "backpack":
			if(num_backpack >= 1) { break; }
			break;
		case "inventory":
			if(num_inventory + slots > 12) { break; }
			num_inventory++;
			$('#inventory_' + num_inventory).html("<img onclick=\"delitem('inventory', " + num_inventory + ", '" + classname + "', " + slots + ")\" style='cursor:pointer; cursor:hand;' src='./items/" + classname + ".png'>");
			secondarystring = secondarystring + "\"" + classname + "\",";
			num_inventory = num_inventory + slots - 1;
			break;
		case "secinventory":
			if(num_secinventory + slots > 8) { break; }
			num_secinventory++;
			$('#secinventory_' + num_secinventory).html("<img onclick=\"delitem('secinventory', " + num_secinventory + ", '" + classname + "', " + slots + ")\" style='cursor:pointer; cursor:hand;' src='./items/" + classname + ".png'>");
			secondarystring = secondarystring + "\"" + classname + "\",";
			num_secinventory = num_secinventory + slots - 1;
			break;
		case "tool":
			if(num_tool + slots> 12) { break; }
			if(primarystring.indexOf(classname, 0) != -1) { break; }
			num_tool++;
			$('#tool_' + num_tool).html("<img onclick=\"delitem('tool', " + num_tool + ", '" + classname + "', " + slots + ")\" style='cursor:pointer; cursor:hand;' src='./items/" + classname + ".png'>");
			primarystring = primarystring + "\"" + classname + "\",";
			num_tool = num_tool + slots - 1;
			break;
	}
}
function delitem(type, slot, classname, slots) {
	switch(type) {
		case "weaponprimary":
			primarystring = primarystring.replace("\"" + classname + "\",", "");
			num_weaponprimary = 0;
			$('#weaponprimary').html("");
			break;
		case "weaponsecondary":
			primarystring = primarystring.replace("\"" + classname + "\",", "");
			num_weaponsecondary = 0;
			$('#weaponsecondary').html("");
			break;
		case "head":
			if(slot == num_head) {
				$('#head_' + slot).html("");
				primarystring = primarystring.replace("\"" + classname + "\",", "");
				num_head = num_head - 1;
			} else { alert('Click the last item added to the category to remove it.'); break; }
			break;
		case "tool":
			if(slot == num_tool + 1 - slots) {
				$('#tool_' + slot).html("");
				primarystring = primarystring.replace("\"" + classname + "\",", "");
				num_tool = num_tool - slots;
			} else { alert('Click the last item added to the category to remove it.'); break; }
			break;
		case "inventory":
			if(slot == num_inventory + 1 - slots) {
				$('#inventory_' + slot).html("");
				secondarystring = secondarystring.replace("\"" + classname + "\",", "");
				num_inventory = num_inventory - slots;
			} else { alert('Click the last item added to the category to remove it.'); break; }
			break;
		case "secinventory":
			if(slot == num_secinventory + 1 - slots) {
				$('#secinventory_' + slot).html("");
				secondarystring = secondarystring.replace("\"" + classname + "\",", "");
				num_secinventory = num_secinventory - slots;
			} else { alert('Click the last item added to the category to remove it.'); break; }
			break;
	}
}
$(document).ready(function() {
	$("#save").click(function() {
        prompt("Loadout String:","[[" + primarystring.substring(0, primarystring.length - 1) + "],[" + secondarystring.substring(0, secondarystring.length - 1) + "]]");
	});

	$("#shownonspawncheck").click( function(){
	if( $(this).is(':checked') ) {
		$("[id=nospawn]").show(); 
	} else {
		$("[id=nospawn]").hide(); 	
	}
	});
<?php
$loadout = "[]";
if($loadout != "[]" && $loadout != "") {
print "infodisplayed = true;";
$fail = false;
$result = mysql_query("SELECT classname,slot_type,slots FROM items");
while($row = mysql_fetch_array($result)) {
$items[$row['classname']] = $row['slot_type'];
$slots[$row['classname']] = $row['slots'];
}
$loadout = explode("[[",$loadout);
if(count($loadout) == 2) { $loadout = $loadout[1]; } else { $fail = true; }
$loadout = explode("]]",$loadout);
if(count($loadout) == 2) { $loadout = $loadout[0]; } else { $fail = true; }
$loadout = explode("],[",$loadout);
if($loadout[1] != "") {
if(count($loadout) == 2) { $loadout = $loadout[0].",".$loadout[1]; } else { $fail = true; }
} else {
$loadout = $loadout[0];
}
$loadout = str_replace("\"", "", $loadout);
$loadout = explode(",",$loadout);
if(count($loadout) <= 0) { $fail = true; }

if($fail == true) { print "alert('There was a problem loading your current loadout, the default has been loaded instead.');"; } else {
foreach($loadout as $itemname) {
print "additem('".$items[$itemname]."', '".$itemname."', ".$slots[$itemname].");";
}
}
}
?>
});
</script>
</head>
<body bgcolor="#423e23" style="margin:0;padding:0;color:white;font-family:sans-serif;">
<table width="726px" height="455px" cellspacing=0 cellpadding=0 bgcolor="#423e23">
<tr>
<td width="363px" valign='top'>
<table width="100%" height="100%" cellspacing=0 cellpadding=3>
<tr height='25px'>
<td>
<select id='itemtype'>
<?php
$types = array("Primary Weapons"=>"weapon_primary","Secondary Weapons"=>"weapon_secondary","Tools"=>"tool","Consumables"=>"consumable","Thrown"=>"thrown");
foreach($types as $key=>$value) {
print "<option value='".$value."'>".$key."</option>";
}
?>
</select>
</td>
<td valign='top' align='right'>
<div id='shownonspawn'><label><input id='shownonspawncheck' type="checkbox"> Show non-spawning</label></div>
</td>
</tr>
<tr>
<td colspan=2 valign='top' bgcolor='#58523c'>
<script>
$(document).ready(function() {
	$("#itemtype").change( function(){
	$("#shownonspawncheck").attr('checked',false);
	switch($(this).val()) {
			<?php
			
			foreach($types as $key=>$value) {
			print 'case "'.$value.'":';
			print '$("#itemlisting").html("';
			$result = mysql_query("SELECT * FROM items WHERE category='".$value."' ORDER BY name ASC");
			while($row = mysql_fetch_array($result)) {
				if($row['spawns'] == 1) {
					print "<a href='#' onClick=\\\"additem('".$row['slot_type']."','".$row['classname']."', ".$row['slots'].");";
					if($row['slot_type'] == "weaponprimary" || $row['slot_type'] == "weaponsecondary") {
						echo " if(infodisplayed == false) { alert('Click the weapon name on the left again to add ammunition for the selected weapon'); infodisplayed = true; }";
						if($row['ammo_class'] != "") {
						$result2 = mysql_query("SELECT * FROM items WHERE classname='".$row['ammo_class']."'");
						$result2 = mysql_fetch_array($result2);
						print " additem('".$result2['slot_type']."','".$result2['classname']."', ".$result2['slots'].");";
						}
						if($row['ammosec_class'] != "") {
						$result2 = mysql_query("SELECT * FROM items WHERE classname='".$row['ammosec_class']."'");
						$result2 = mysql_fetch_array($result2);
						print " additem('".$result2['slot_type']."','".$result2['classname']."', ".$result2['slots'].");";
						}
					}
					print "\\\">".$row['name']."</a><br>";
				} else {
					print "<div id='nospawn' style='display:none'><a href='#' onClick=\\\"additem('".$row['slot_type']."','".$row['classname']."', ".$row['slots'].");";
					if($row['slot_type'] == "weaponprimary" || $row['slot_type'] == "weaponsecondary") {
						echo " if(infodisplayed == false) { alert('Click the weapon name on the left again to add ammunition for the selected weapon'); infodisplayed = true; }";
						if($row['ammo_class'] != "") {
						$result2 = mysql_query("SELECT * FROM items WHERE classname='".$row['ammo_class']."'");
						$result2 = mysql_fetch_array($result2);
						print " additem('".$result2['slot_type']."','".$result2['classname']."', ".$result2['slots'].");";
						}
						if($row['ammosec_class'] != "") {
						$result2 = mysql_query("SELECT * FROM items WHERE classname='".$row['ammosec_class']."'");
						$result2 = mysql_fetch_array($result2);
						print " additem('".$result2['slot_type']."','".$result2['classname']."', ".$result2['slots'].");";
						}
					}
					print "\\\">".$row['name']."</a> *<br></div>";
				}
			}
			print '");';
			print "break;";
			}
			?>
	}
	});
});
</script>
<div id='itemlisting' style='height: 419px; overflow: auto;'>
<?php
$result = mysql_query("SELECT * FROM items WHERE category='weapon_primary' ORDER BY name ASC");
while($row = mysql_fetch_array($result)) {
				if($row['spawns'] == 1) {
					print "<a href='#' onClick=\"additem('".$row['slot_type']."','".$row['classname']."', ".$row['slots'].");";
					if($row['slot_type'] == "weaponprimary" || $row['slot_type'] == "weaponsecondary") {
						echo " if(infodisplayed == false) { alert('Click the weapon name on the left again to add ammunition for the selected weapon'); infodisplayed = true; }";
						if($row['ammo_class'] != "") {
						$result2 = mysql_query("SELECT * FROM items WHERE classname='".$row['ammo_class']."'");
						$result2 = mysql_fetch_array($result2);
						print " additem('".$result2['slot_type']."','".$result2['classname']."', ".$result2['slots'].");";
						}
						if($row['ammosec_class'] != "") {
						$result2 = mysql_query("SELECT * FROM items WHERE classname='".$row['ammosec_class']."'");
						$result2 = mysql_fetch_array($result2);
						print " additem('".$result2['slot_type']."','".$result2['classname']."', ".$result2['slots'].");";
						}
					}
					print "\">".$row['name']."</a><br>";
				} else {
					print "<div id='nospawn' style='display:none'><a href='#' onClick=\"additem('".$row['slot_type']."','".$row['classname']."', ".$row['slots'].");";
					if($row['slot_type'] == "weaponprimary" || $row['slot_type'] == "weaponsecondary") {
						echo " if(infodisplayed == false) { alert('Click the weapon name on the left again to add ammunition for the selected weapon'); infodisplayed = true; }";
						if($row['ammo_class'] != "") {
						$result2 = mysql_query("SELECT * FROM items WHERE classname='".$row['ammo_class']."'");
						$result2 = mysql_fetch_array($result2);
						print " additem('".$result2['slot_type']."','".$result2['classname']."', ".$result2['slots'].");";
						}
						if($row['ammosec_class'] != "") {
						$result2 = mysql_query("SELECT * FROM items WHERE classname='".$row['ammosec_class']."'");
						$result2 = mysql_fetch_array($result2);
						print " additem('".$result2['slot_type']."','".$result2['classname']."', ".$result2['slots'].");";
						}
					}
					print "\">".$row['name']."</a> *<br></div>";
				}
}
?>
</div>
</td>
</tr>
</table>
</td>
<td width="363px" style="background-image:url('items/inventory.png')">
<div id='itempane' style='position:relative; height:455px; width:363px;'>
<div id='head_1' style='position:absolute; top:0px; left:0px; width:89px; height:89px'></div>
<div id='head_2' style='position:absolute; top:0px; right:1px; width:89px; height:89px'></div>
<div id='weaponprimary' style='position:absolute; top:91px; left:0px; width:277px; height:90px'></div>
<div id='backpack' style='position:absolute; top:182px; left:0px; width:277px; height:90px'></div>
<div id='weaponsecondary' style='position:absolute; top:274px; left:46px; width:89px; height:89px;'></div>
<div id='inventory_1' style='position:absolute; top:91px; right:91px; width:44px; height:44px'></div>
<div id='inventory_2' style='position:absolute; top:91px; right:46px; width:44px; height:44px'></div>
<div id='inventory_3' style='position:absolute; top:91px; right:1px; width:44px; height:44px'></div>
<div id='inventory_4' style='position:absolute; top:136px; right:91px; width:44px; height:44px'></div>
<div id='inventory_5' style='position:absolute; top:136px; right:46px; width:44px; height:44px'></div>
<div id='inventory_6' style='position:absolute; top:136px; right:1px; width:44px; height:44px'></div>
<div id='inventory_7' style='position:absolute; top:182px; right:91px; width:44px; height:44px'></div>
<div id='inventory_8' style='position:absolute; top:182px; right:46px; width:44px; height:44px'></div>
<div id='inventory_9' style='position:absolute; top:182px; right:1px; width:44px; height:44px'></div>
<div id='inventory_10' style='position:absolute; top:228px; right:91px; width:44px; height:44px'></div>
<div id='inventory_11' style='position:absolute; top:228px; right:46px; width:44px; height:44px'></div>
<div id='inventory_12' style='position:absolute; top:228px; right:1px; width:44px; height:44px'></div>
<div id='secinventory_1' style='position:absolute; top:274px; left:136px; width:44px; height:44px'></div>
<div id='secinventory_2' style='position:absolute; top:274px; left:183px; width:44px; height:44px'></div>
<div id='secinventory_3' style='position:absolute; top:274px; left:228px; width:44px; height:44px'></div>
<div id='secinventory_4' style='position:absolute; top:274px; left:273px; width:44px; height:44px'></div>
<div id='secinventory_5' style='position:absolute; top:319px; left:136px; width:44px; height:44px'></div>
<div id='secinventory_6' style='position:absolute; top:319px; left:183px; width:44px; height:44px'></div>
<div id='secinventory_7' style='position:absolute; top:319px; left:228px; width:44px; height:44px'></div>
<div id='secinventory_8' style='position:absolute; top:319px; left:273px; width:44px; height:44px'></div>
<div id='tool_1' style='position:absolute; top:365px; left:46px; width:44px; height:44px'></div>
<div id='tool_2' style='position:absolute; top:365px; left:91px; width:44px; height:44px'></div>
<div id='tool_3' style='position:absolute; top:365px; left:136px; width:44px; height:44px'></div>
<div id='tool_4' style='position:absolute; top:365px; left:183px; width:44px; height:44px'></div>
<div id='tool_5' style='position:absolute; top:365px; left:228px; width:44px; height:44px'></div>
<div id='tool_6' style='position:absolute; top:365px; left:273px; width:44px; height:44px'></div>
<div id='tool_7' style='position:absolute; top:410px; left:46px; width:44px; height:44px'></div>
<div id='tool_8' style='position:absolute; top:410px; left:91px; width:44px; height:44px'></div>
<div id='tool_9' style='position:absolute; top:410px; left:136px; width:44px; height:44px'></div>
<div id='tool_10' style='position:absolute; top:410px; left:183px; width:44px; height:44px'></div>
<div id='tool_11' style='position:absolute; top:410px; left:228px; width:44px; height:44px'></div>
<div id='tool_12' style='position:absolute; top:410px; left:273px; width:44px; height:44px'></div>

</div>
</td>
</tr>
<tr><td colspan=2 align='center'><input id='save' type='button' value='Save Loadout'></td></tr>
</table>

<?php } mysql_close();
?>