<h1>Login</h1>

<?php

if (isset($_GET["out"]) AND $_GET["out"] == "1") {

    $_SESSION = array();
    echo "You are logged out.";
    echo "<meta http-equiv=\"Refresh\" content=\"0; url=./\">";
    exit;

}
if ($loggedin === true) echo "You are logged in.";
else {

    $showform = 1;
    $error = "";
    $showloggedmessage = 0;
    if ($_SERVER['REQUEST_METHOD'] == "POST") {

    	if (!ctype_alnum($_POST['unique_id'])):
            $error .= "Invalid characters in the Unique ID.";
            $showform = 1;
        endif;

    	if ($_POST['unique_id'] == "" OR $_POST['name'] == ""):
            $error .= "Unique ID or Name are too short.";
            $showform = 1;
        endif;

    	$unique_id = $_POST['unique_id'];
    	$name = $_POST['name'];

        if ($error == "") {

            $query = $dbh->prepare("SELECT * FROM profile WHERE unique_id = ? AND name = ?" . " AND (rights = 'admin' OR rights = 'premium' OR rights = 'member')  LIMIT 1");
            $query->execute(array($unique_id, $name));
            if ($query->rowCount() > 0) {

                $user = $query->fetch();
     			$_SESSION["loggedin"] = md5($user["unique_id"].$user["name"]);
     			$_SESSION["adminid"] = $user["unique_id"];
			$_SESSION["username"] = $user["name"];
                $showloggedmessage = 1;
                $showform = 0;
                echo "<meta http-equiv=\"Refresh\" content=\"0; url=./\">";

            } else {

                $error .= "Invalid Unique Id or Name.";
                $showform = 1;

            }

        }

    }

    if ($error !== ''){
        echo "<div class='panel panel-danger'>";
        echo "<div class='panel-heading'>";
        echo "<h3 class='panel-title'>Error</h3></div>";
        echo "<div class='panel-body'>$error</div></div>";
    }
    if ($showform === 1) {

        ?>

        <form class="form-horizontal" role="form" autocomplete="on" method="post" action="./?p=login">
            <div class="form-group">
                <label for="inputEmail" class="col-lg-2 control-label">Ingame Name</label>
                <div class="col-lg-3">
                    <input type="text" name="name" class="form-control" id="inputEmail" placeholder="Ingame Name">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-lg-2 control-label">Unique ID</label>
                <div class="col-lg-3">
                    <input type="password" name="unique_id" class="form-control" id="inputPassword" placeholder="Unique ID">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-10">
                    <button type="submit" name="submit" class="btn btn-default">Sign in</button>
                </div>
            </div>
        </form>

        <?php

    } else {

        echo "<h4>Success</h4>You have been successfully logged in.";
        
    }

}

?>
